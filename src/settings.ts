// 修改导航栏标题
const defaultSettings: AppSettings = {
  title: "我是vue3 TS 测试项目",
  version: "v2.9.1",
  showSettings: true,
  tagsView: true,
  fixedHeader: false,
  sidebarLogo: true,
  layout: "left",
  theme: "light",
  size: "default",
  language: "zh-cn",
  themeColor: "#409EFF",
  watermarkEnabled: false,
  watermarkContent: "vue3-element-admin",
};

export default defaultSettings;
